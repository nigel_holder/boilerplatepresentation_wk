﻿using Abp.Web.Mvc.Views;

namespace WKMVCBoilerPlate.Web.Views
{
    public abstract class WKMVCBoilerPlateWebViewPageBase : WKMVCBoilerPlateWebViewPageBase<dynamic>
    {

    }

    public abstract class WKMVCBoilerPlateWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected WKMVCBoilerPlateWebViewPageBase()
        {
            LocalizationSourceName = WKMVCBoilerPlateConsts.LocalizationSourceName;
        }
    }
}