﻿using Abp.AutoMapper;
using WKMVCBoilerPlate.Sessions.Dto;

namespace WKMVCBoilerPlate.Web.Models.Account
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}