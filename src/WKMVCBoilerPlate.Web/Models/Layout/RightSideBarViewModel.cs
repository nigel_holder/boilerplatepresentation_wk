using WKMVCBoilerPlate.Configuration.Ui;

namespace WKMVCBoilerPlate.Web.Models.Layout
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}