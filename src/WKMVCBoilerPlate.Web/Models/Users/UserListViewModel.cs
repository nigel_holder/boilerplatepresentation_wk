﻿using System.Collections.Generic;
using WKMVCBoilerPlate.Roles.Dto;
using WKMVCBoilerPlate.Users.Dto;

namespace WKMVCBoilerPlate.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}