﻿using System.Collections.Generic;
using WKMVCBoilerPlate.Roles.Dto;

namespace WKMVCBoilerPlate.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}