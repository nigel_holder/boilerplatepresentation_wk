﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace WKMVCBoilerPlate.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : WKMVCBoilerPlateControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}