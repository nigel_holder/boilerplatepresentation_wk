﻿using Abp.MultiTenancy;
using WKMVCBoilerPlate.Authorization.Users;

namespace WKMVCBoilerPlate.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}