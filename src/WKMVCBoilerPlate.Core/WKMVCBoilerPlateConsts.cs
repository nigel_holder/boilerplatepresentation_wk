﻿namespace WKMVCBoilerPlate
{
    public class WKMVCBoilerPlateConsts
    {
        public const string LocalizationSourceName = "WKMVCBoilerPlate";

        public const bool MultiTenancyEnabled = true;
    }
}