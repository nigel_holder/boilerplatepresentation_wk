﻿using Abp.Authorization;
using WKMVCBoilerPlate.Authorization.Roles;
using WKMVCBoilerPlate.Authorization.Users;

namespace WKMVCBoilerPlate.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
