﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WKMVCBoilerPlate.MultiTenancy.Dto;

namespace WKMVCBoilerPlate.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
