﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WKMVCBoilerPlate.Authorization.Accounts.Dto;

namespace WKMVCBoilerPlate.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
