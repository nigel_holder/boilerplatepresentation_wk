using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WKMVCBoilerPlate.Roles.Dto;
using WKMVCBoilerPlate.Users.Dto;

namespace WKMVCBoilerPlate.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UpdateUserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
    }
}