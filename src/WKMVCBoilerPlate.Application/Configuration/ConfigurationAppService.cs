﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using WKMVCBoilerPlate.Configuration.Dto;

namespace WKMVCBoilerPlate.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : WKMVCBoilerPlateAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
