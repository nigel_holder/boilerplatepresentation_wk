﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WKMVCBoilerPlate.Configuration.Dto;

namespace WKMVCBoilerPlate.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}