﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WKMVCBoilerPlate.Sessions.Dto;

namespace WKMVCBoilerPlate.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
