using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using WKMVCBoilerPlate.EntityFramework;

namespace WKMVCBoilerPlate.Migrator
{
    [DependsOn(typeof(WKMVCBoilerPlateDataModule))]
    public class WKMVCBoilerPlateMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<WKMVCBoilerPlateDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}