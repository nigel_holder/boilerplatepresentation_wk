﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using WKMVCBoilerPlate.EntityFramework;

namespace WKMVCBoilerPlate
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(WKMVCBoilerPlateCoreModule))]
    public class WKMVCBoilerPlateDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<WKMVCBoilerPlateDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
