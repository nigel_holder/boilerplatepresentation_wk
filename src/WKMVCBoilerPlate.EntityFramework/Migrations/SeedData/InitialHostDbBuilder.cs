﻿using WKMVCBoilerPlate.EntityFramework;
using EntityFramework.DynamicFilters;

namespace WKMVCBoilerPlate.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly WKMVCBoilerPlateDbContext _context;

        public InitialHostDbBuilder(WKMVCBoilerPlateDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
