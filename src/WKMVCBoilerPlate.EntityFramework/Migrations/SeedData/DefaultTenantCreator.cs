using System.Linq;
using WKMVCBoilerPlate.EntityFramework;
using WKMVCBoilerPlate.MultiTenancy;

namespace WKMVCBoilerPlate.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly WKMVCBoilerPlateDbContext _context;

        public DefaultTenantCreator(WKMVCBoilerPlateDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
