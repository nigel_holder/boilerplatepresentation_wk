﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace WKMVCBoilerPlate.EntityFramework.Repositories
{
    public abstract class WKMVCBoilerPlateRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<WKMVCBoilerPlateDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected WKMVCBoilerPlateRepositoryBase(IDbContextProvider<WKMVCBoilerPlateDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class WKMVCBoilerPlateRepositoryBase<TEntity> : WKMVCBoilerPlateRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected WKMVCBoilerPlateRepositoryBase(IDbContextProvider<WKMVCBoilerPlateDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
