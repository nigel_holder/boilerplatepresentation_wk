﻿using System.Data.Common;
using Abp.Zero.EntityFramework;
using WKMVCBoilerPlate.Authorization.Roles;
using WKMVCBoilerPlate.Authorization.Users;
using WKMVCBoilerPlate.MultiTenancy;

namespace WKMVCBoilerPlate.EntityFramework
{
    public class WKMVCBoilerPlateDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //TODO: Define an IDbSet for your Entities...

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public WKMVCBoilerPlateDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in WKMVCBoilerPlateDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of WKMVCBoilerPlateDbContext since ABP automatically handles it.
         */
        public WKMVCBoilerPlateDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public WKMVCBoilerPlateDbContext(DbConnection existingConnection)
         : base(existingConnection, false)
        {

        }

        public WKMVCBoilerPlateDbContext(DbConnection existingConnection, bool contextOwnsConnection)
         : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}
